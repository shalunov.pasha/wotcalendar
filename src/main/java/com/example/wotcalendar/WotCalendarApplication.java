package com.example.wotcalendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WotCalendarApplication {

    public static void main(String[] args) {
        SpringApplication.run(WotCalendarApplication.class, args);
    }
}
