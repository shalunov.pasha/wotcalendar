package com.example.wotcalendar;

import com.example.wotcalendar.pages.HomePage;
import com.example.wotcalendar.pages.PageFactory;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Feature("Календарь")
class WotCalendarApplicationTests {

    private final PageFactory pageFactory;

    @Value("${email}")
    private String email;
    @Value("${password}")
    private String password;

    @Autowired
    public WotCalendarApplicationTests(PageFactory pageFactory) {
        this.pageFactory = pageFactory;
    }
   @Test
   @Description("Кликаем на сегодняшний день")
   @DisplayName("Название: Кликаем на сегодняшний день")
    public void testLogin() throws InterruptedException {
        HomePage homePage = pageFactory.createHomePage();
        homePage.btnClose().clickCalendar();
        homePage.logIn()
                .insertEmail(email).insertPassword(password).clickBtnLogIn()
                .clickTodayData();
        Thread.sleep(5000);
   }
}
