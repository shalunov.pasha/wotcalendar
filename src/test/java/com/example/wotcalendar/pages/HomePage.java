package com.example.wotcalendar.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import io.qameta.allure.Step;

public class HomePage extends BasePage {
    public HomePage(Page page) {
        super(page);
    }

    CalendarPage calendarPage = new CalendarPage(page);
    SigninPage signinPage = new SigninPage(page);

    public Locator btnClose = page.getByRole(AriaRole.BUTTON);
    public Locator logIn = page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Войти"));

    public Locator calendar = page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Табель-календарь").setExact(true));

    @Step("Закрыть рекламу")
    public HomePage btnClose() {
        System.out.println("Click close btn");
        this.btnClose.click();
        return this;
    }

    @Step("Перейти на календарь")
    public CalendarPage clickCalendar() {
        System.out.println("go page calendar");
        this.calendar.click();
        return calendarPage;
    }

    @Step("Перейти на авторизацию")
    public SigninPage logIn() {
        System.out.println("go page logIn");
        this.logIn.click();
        return signinPage;
    }
}
