package com.example.wotcalendar.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import io.qameta.allure.Step;

import java.nio.file.Paths;

public class SigninPage extends BasePage{
    public SigninPage(Page page) {
        super(page);
    }

    public Locator email = page.getByPlaceholder("Электронная почта");
    public Locator password = page.getByPlaceholder("Пароль");
    public Locator btnLogIn = page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Войти"));

    @Step
    public SigninPage insertEmail(String email) throws InterruptedException {
        System.out.println("insert email");
        Thread.sleep(3*1000);
        this.email.fill(email);
        return this;
    }

    @Step
    public SigninPage insertPassword(String pass) throws InterruptedException {
        System.out.println("insert password");
        Thread.sleep(3*1000);
        this.password.fill(pass);

        return this;
    }

    @Step
    public CalendarPage clickBtnLogIn() throws InterruptedException {
        System.out.println("input btn login");
        Thread.sleep(2*1000);
        this.btnLogIn.click();
        page.screenshot(new Page.ScreenshotOptions().setPath(Paths.get("target/screenshot.png")));
        return new CalendarPage(page);
    }
}
