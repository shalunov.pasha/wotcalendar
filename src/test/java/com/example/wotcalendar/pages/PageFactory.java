package com.example.wotcalendar.pages;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.ScreenSize;
import org.springframework.stereotype.Component;

@Component
public class PageFactory {
    private static final String BASE_URL = "https://tanki.su";

    public static HomePage createHomePage() {
        Playwright pw = Playwright.create();
        Browser browser = pw.chromium()
                .launch(new BrowserType.LaunchOptions().setHeadless(true));
        Page page = browser.newPage();
        page.setViewportSize(1900, 900);
        page.navigate(BASE_URL);
        return new HomePage(page);
    }
}
