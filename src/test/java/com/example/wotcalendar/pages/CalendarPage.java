package com.example.wotcalendar.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CalendarPage extends BasePage{
    public CalendarPage(Page page) {
        super(page);
    }


    public Locator data = page.locator("div:nth-child(" + getTodayDate() + ") > .c_item__body");

    public Integer getTodayDate() {
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("d");
        return Integer.valueOf(formatForDateNow.format(dateNow));
    }

    @Step
    public CalendarPage clickTodayData () throws InterruptedException {
        System.out.println("click data");
        Thread.sleep(30*1000);
        page.screenshot(new Page.ScreenshotOptions().setPath(Paths.get("target/clickData.png")));
        this.data.click();
        page.screenshot(new Page.ScreenshotOptions().setPath(Paths.get("target/clickData30.png")));
        return this;
    }
}
