package com.example.wotcalendar.pages;

import com.microsoft.playwright.Page;
import io.qameta.allure.Step;

public class BasePage {

    protected Page page;

    public BasePage(Page page) {
        this.page = page;
    }


    @Step
    public void navigate(String url) {
        page.navigate(url);
    }

    @Step
    public  void  updatePage() {
        page.reload();
    }
//
//    Locator loginLink = page.getByText("Войти");

}
